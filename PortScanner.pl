#!/usr/bin/perl
# Simple Perl Port Scanner 

use strict;
use warnings;

use feature qw(say);
use Time::HiRes qw(time);
use Getopt::Std;
use IO::Socket;
use IO::Handle;

STDOUT->autoflush;

my $top_ports_file = "top_ports.txt";

# Get host name or print usage and quit
my $host = shift @ARGV or usage();

# Get options
our ($opt_p);
getopt("p:");

# Load top used ports
my %ports_info;
if (-e $top_ports_file) {
    open my $top_ports, "<", $top_ports_file or die("Could not open $top_ports_file: $!");
    while (<$top_ports>) {
        my ($port, $description) = (m/^(\d+) (.+)$/);
        $ports_info{$port} = $description;       
    }
}

say "Running PortScanner @ " . localtime . "...";

# Check if the host is alive
ping($host);

# Prepare an array of ports to scan
my @ports_list = get_ports_lists($opt_p);
if (@ports_list) {
    say "Scanning $host ports in given ranges.";
} else {
    @ports_list = sort { $a <=> $b } keys %ports_info;
    say "Scanning well known ports of $host.";
}

my $ports_found = 0;
my $start_time = time;
for my $port (@ports_list) {
    print "\rPort $port...";
    if (scan($host, $port)) {
        say "\rPort $port (" , $ports_info{$port} // "unknown"  , ") OPEN!";
        $ports_found++;
    }
}

# Print elapsed time
my $elapsed_time = time - $start_time;
printf("\rFound $ports_found open port(s). Finished in %.2f seconds.\n", $elapsed_time);

sub ping {
    $host = shift;

    my $ping = `ping -c 1 $host 2>/dev/null`;
    my $status = $? >> 8;

    # Search for IP
    my ($ip) = $ping =~ m/\(((\d{1,3}\.){3}\d{1,3})\)/;
    $ip = "0.0.0.0" unless defined $ip;

    # Search for time
    my ($duration) = $ping =~ m/time=(.+?) ms/;
    $duration = 0.0 unless defined $duration;

    # Was the ping successful? (return code != 0)
    die("Could not reach the host!") if $status;

    printf("Host (IP: %s) is alive (%.2f ms).\n", $ip, $duration);
}

sub scan {
    my ($host, $port) = @_;

    my $sock = new IO::Socket::INET (
        PeerHost => $host,
        PeerPort => $port,
        Proto => 'tcp',
        Timeout => 0.5,
    );

    $sock->close() if defined $sock;
    return $sock;
}

# Returns an array of ports to scan
sub get_ports_lists {
    my $input_list = shift or return;

    my @list;

    for my $range (split /,/, $input_list) {
        # Is it a range or single entry?
        if ($range =~ m/^(\d+)-(\d+)$/) {
            # Max port > min port
            next if $2 < $1;
            push @list, ($1 .. $2);
        } elsif ($range =~ m/\d+/) {
            push @list, $range;
        }
    }

    return @list;
}

sub usage {
    say "Usage: $0 <Host> [-p StartPort[-EndPort]]";
    exit 1;
}
